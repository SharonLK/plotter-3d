#version 120

uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 position;
in vec3 color;

out vec3 passColor;

void main() {
    gl_Position = perspectiveMatrix * viewMatrix * modelMatrix * vec4(position, 1);

    passColor = color;
}
