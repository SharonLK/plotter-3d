#version 120

uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 position;
in vec3 normal1;
in vec3 normal2;

out float passHeight;
out vec3 passNormal1;
out vec3 passNormal2;
out vec3 passPosition;

void main() {
    gl_Position = perspectiveMatrix * viewMatrix * modelMatrix * vec4(position, 1);

    passNormal1 = mat3(modelMatrix) * normal1;
    passNormal2 = mat3(modelMatrix) * normal2;
    passPosition = mat3(modelMatrix) * position;

    passHeight = position.y;
}
