#version 120

in vec3 passColor;

out vec4 outColor;

void main() {
    outColor = vec4(outColor, 1);
}
