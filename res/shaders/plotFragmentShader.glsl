#version 120

#define MAX_LIGHTS 10

struct PointLight {
    vec3 position;
    vec3 color;
};

uniform PointLight lights[MAX_LIGHTS];
uniform int numLights;
uniform float minValue;
uniform float maxValue;
uniform int levels;

in float passHeight;
in vec3 passNormal1;
in vec3 passNormal2;
in vec3 passPosition;

out vec4 outColor;

vec3 addLight(PointLight light, vec3 color) {
    vec3 toLight = normalize(light.position - passPosition);
    float coefficient1 = dot(toLight, normalize(passNormal1));
    coefficient1 = max(0.0, coefficient1);

    float coefficient2 = dot(toLight, normalize(passNormal2));
    coefficient2 = max(0.0, coefficient2);

    return coefficient1 * color * light.color + coefficient2 * color * light.color + 0.1 * color * light.color;
}

void main() {
    float red = ((1 - 2 * ((maxValue - passHeight) / (maxValue - minValue)))) * 2;
    float green = 2 * ((maxValue - passHeight) / (maxValue - minValue));
    red = floor(red * levels) / levels;
    green = floor(green * levels) / levels;

    vec3 color = vec3(red, green, 0);
    if(red < 0) {
        float mid = (maxValue + minValue) / 2;

        float green = ((1 - 2 * ((mid - passHeight) / (mid - minValue))));
        float blue = 2 * ((mid - passHeight) / (mid - minValue));

        color = vec3(0, green, blue);
    }

    vec3 finalColor = vec3(0, 0, 0);

    for(int i = 0; i < numLights; i++) {
        finalColor += addLight(lights[i], color);
    }

    outColor = vec4(finalColor, 1);
}
