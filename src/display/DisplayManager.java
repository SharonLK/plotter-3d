package display;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 * Created by Sharon on 14-Dec-15.
 */
public enum DisplayManager {
    ;

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;

    public static int FPS = 60;
    public static Vector3f backColor = new Vector3f(0, 0.2f, 0.2f);

    /**
     * Creates a new OpenGL context window with dimensions of WIDTH and HEIGHT.
     */
    public static void create() {
        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

//        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    /**
     * @return Whether or not the user has requested to close the window.
     */
    public static boolean iscCloseRequested() {
        return Display.isCloseRequested();
    }

    /**
     * Clears the color buffer and sets the background.
     */
    public static void clear() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(backColor.x, backColor.y, backColor.z, 1);
    }

    /**
     * Updates the display.
     */
    public static void update() {
        Display.update();
        Display.sync(FPS);
    }

    /**
     * Destroys the window.
     */
    public static void destroy() {
        Display.destroy();
    }
}
