import display.DisplayManager;
import environment.PointLight;
import model.ModelsLoader;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import plot.display.Plot;
import plot.PlotParenthesisDontMatchException;
import plot.PlotParseException;
import plot.PlotProcessor;
import render.Camera;
import render.Renderer;

import java.util.ArrayList;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Plotter {
    private static long lastFrameMillis;

    private static Camera camera;
    private static Plot plot;

    public static void main(String[] args) {
        DisplayManager.create();
        lastFrameMillis = getMillis();

        try {
            plot = PlotProcessor.getEquationModel("(x^2+y^2+2^(x*0.09+y*0.09))*0.01", -50, 50, 10, -50, 50, 10, 255,
                    new Vector3f(0, 0, 0), new Vector3f(0, 0, 0), new Vector3f(1, 1, 1));
        } catch (PlotParenthesisDontMatchException | PlotParseException e) {
            e.printStackTrace();

            return;
        }

        camera = new Camera(new Vector3f(-100, 100, 100), 45, 45, 0,
                (float) DisplayManager.WIDTH / (float) DisplayManager.HEIGHT, 70, 10, -1000);
        Renderer.setCamera(camera);

        ArrayList<PointLight> lights = new ArrayList<>();
        lights.add(new PointLight(new Vector3f(200, 200, 0), new Vector3f(1, 1, 1)));

        while (!DisplayManager.iscCloseRequested()) {
            DisplayManager.clear();
            int delta = getDeltaMillis();

            processMouse(delta);
            processKeyboard(delta);

            Vector3f plotRotation = plot.getRotation();
            plotRotation.z += 0;
            plot.setRotation(plotRotation);

            Renderer.loadLights(lights);
            Renderer.renderPlot(plot);

            DisplayManager.update();
        }

        ModelsLoader.clean();
        Renderer.clean();

        DisplayManager.destroy();
    }


    private static void processMouse(int delta) {
        if (Mouse.isButtonDown(0)) {
            int dy = Mouse.getDY();
            int dx = Mouse.getDX();

            camera.setPitch(camera.getPitch() - dy * 0.015f * delta);
            camera.setYaw(camera.getYaw() + dx * 0.015f * delta);
        }
    }

    private static void processKeyboard(int delta) {
        Vector3f cameraPos = camera.getPosition();
        Vector3f cameraForward = camera.getForward();
        cameraForward.scale(0.1f * delta);
        Vector3f cameraRight = camera.getRight();
        cameraRight.scale(0.1f * delta);

        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            Vector3f.add(cameraPos, cameraForward, cameraPos);
            camera.setPosition(cameraPos);
        } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            Vector3f.sub(cameraPos, cameraForward, cameraPos);
            camera.setPosition(cameraPos);
        }

        cameraPos = camera.getPosition();

        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            Vector3f.add(cameraPos, cameraRight, cameraPos);
            camera.setPosition(cameraPos);
        } else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            Vector3f.sub(cameraPos, cameraRight, cameraPos);
            camera.setPosition(cameraPos);
        }

        if (plot != null) {
            Vector3f rotation = plot.getRotation();

            if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
                rotation.y -= 0.1f * delta;
                plot.setRotation(rotation);
            } else if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
                rotation.y += 0.1f * delta;
                plot.setRotation(rotation);
            }
        }
    }


    private static int getDeltaMillis() {
        long currentMillis = getMillis();
        int delta = (int) (currentMillis - lastFrameMillis);
        lastFrameMillis = currentMillis;

        return delta;
    }

    private static long getMillis() {
        return System.nanoTime() / 1000000;
    }
}
