package render.shader;

import environment.Light;
import org.lwjgl.util.vector.Matrix4f;

import java.util.List;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class PlotShader extends ShaderProgram {
    private static final int MAX_LIGHTS = 10;
    private static final String VERTEX_SHADER = "res/shaders/plotVertexShader.glsl";
    private static final String FRAGMENT_SHADER = "res/shaders/plotFragmentShader.glsl";

    private int locPerspectiveMatrix;
    private int locViewMatrix;
    private int locModelMatrix;

    private int locMinimumValue;
    private int locMaximumValue;
    private int locLevels;

    private int locNumLights;
    private int[] locLightPositions;
    private int[] locLightColors;

    public PlotShader() {
        super(VERTEX_SHADER, FRAGMENT_SHADER);
    }

    @Override
    protected void getAllUniformLocations() {
        locPerspectiveMatrix = getUniformLocation("perspectiveMatrix");
        locViewMatrix = getUniformLocation("viewMatrix");
        locModelMatrix = getUniformLocation("modelMatrix");

        locMinimumValue = getUniformLocation("minValue");
        locMaximumValue = getUniformLocation("maxValue");
        locLevels = getUniformLocation("levels");

        locNumLights = getUniformLocation("numLights");
        locLightPositions = new int[MAX_LIGHTS];
        locLightColors = new int[MAX_LIGHTS];
        for (int i = 0; i < MAX_LIGHTS; i++) {
            locLightPositions[i] = getUniformLocation("lights[" + i + "].position");
            locLightColors[i] = getUniformLocation("lights[" + i + "].color");
        }
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "normal1");
        bindAttribute(2, "normal2");
    }

    public void loadPerspectiveMatrix(Matrix4f perspectiveMatrix) {
        loadMatrix(locPerspectiveMatrix, perspectiveMatrix);
    }

    public void loadViewMatrix(Matrix4f viewMatrix) {
        loadMatrix(locViewMatrix, viewMatrix);
    }

    public void loadModelMatrix(Matrix4f modelMatrix) {
        loadMatrix(locModelMatrix, modelMatrix);
    }

    public void loadPlotParameters(float minimumValue, float maximumValue, int levels) {
        loadFloat(locMinimumValue, minimumValue);
        loadFloat(locMaximumValue, maximumValue);
        loadInt(locLevels, levels);
    }

    public void loadLights(List<? extends Light> lights) {
        loadInt(locNumLights, lights.size());

        for (int i = 0; i < lights.size(); i++) {
            loadVector3f(locLightPositions[i], lights.get(i).getPosition());
            loadVector3f(locLightColors[i], lights.get(i).getColor());
        }
    }
}
