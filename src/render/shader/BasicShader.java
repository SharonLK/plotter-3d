package render.shader;

import org.lwjgl.util.vector.Matrix4f;

/**
 * Created by Sharon on 24-Dec-15.
 */
public class BasicShader extends ShaderProgram {
    private static final String VERTEX_SHADER = "res/shaders/basicVertexShader.glsl";
    private static final String FRAGMENT_SHADER = "res/shaders/basicFragmentShader.glsl";

    private int locPerspectiveMatrix;
    private int locViewMatrix;
    private int locModelMatrix;

    public BasicShader() {
        super(VERTEX_SHADER, FRAGMENT_SHADER);
    }

    @Override
    protected void getAllUniformLocations() {
        locPerspectiveMatrix = getUniformLocation("perspectiveMatrix");
        locViewMatrix = getUniformLocation("viewMatrix");
        locModelMatrix = getUniformLocation("modelMatrix");
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "color");
    }

    public void loadPerspectiveMatrix(Matrix4f perspectiveMatrix) {
        loadMatrix(locPerspectiveMatrix, perspectiveMatrix);
    }

    public void loadViewMatrix(Matrix4f viewMatrix) {
        loadMatrix(locViewMatrix, viewMatrix);
    }

    public void loadModelMatrix(Matrix4f modelMatrix) {
        loadMatrix(locModelMatrix, modelMatrix);
    }
}
