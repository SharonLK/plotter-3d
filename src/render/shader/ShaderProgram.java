package render.shader;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

/**
 * Created by Sharon on 14-Dec-15.
 */
public abstract class ShaderProgram {
    private int programID;
    private int vertexShaderID;
    private int fragmentShaderID;

    public ShaderProgram(String vertexShaderPath, String fragmentShaderPath) {
        // Load both vertex and fragment shaders from given files
        vertexShaderID = loadShader(vertexShaderPath, GL20.GL_VERTEX_SHADER);
        fragmentShaderID = loadShader(fragmentShaderPath, GL20.GL_FRAGMENT_SHADER);

        // Create an empty program
        programID = GL20.glCreateProgram();

        // Attach shaders to program
        GL20.glAttachShader(programID, vertexShaderID);
        GL20.glAttachShader(programID, fragmentShaderID);

        // Bind attributes
        bindAttributes();

        // Link and validate the current program
        GL20.glLinkProgram(programID);
        GL20.glValidateProgram(programID);

        getAllUniformLocations();
    }

    protected int getUniformLocation(String name) {
        return GL20.glGetUniformLocation(programID, name);
    }

    protected abstract void getAllUniformLocations();

    protected void loadFloat(int location, float value) {
        GL20.glUniform1f(location, value);
    }

    protected void loadInt(int location, int value) {
        GL20.glUniform1i(location, value);
    }

    protected void loadVector2f(int location, Vector2f vec) {
        GL20.glUniform2f(location, vec.x, vec.y);
    }

    protected void loadVector3f(int location, Vector3f vec) {
        GL20.glUniform3f(location, vec.getX(), vec.getY(), vec.getZ());
    }

    protected void loadBoolean(int location, boolean value) {
        GL20.glUniform1f(location, value ? 1.0f : 0.0f);
    }

    protected void loadMatrix(int location, Matrix4f mat) {
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(16);
        mat.store(floatBuffer);
        floatBuffer.flip();

        GL20.glUniformMatrix4(location, false, floatBuffer);
    }

    public void start() {
        // Change the current used program to this program
        GL20.glUseProgram(programID);
    }

    public void stop() {
        // Stop used the current program
        GL20.glUseProgram(0);
    }

    public void clean() {
        // Stop using this program
        stop();

        // Detach both shaders from the current program and delete them, along with the program
        GL20.glDetachShader(programID, vertexShaderID);
        GL20.glDetachShader(programID, fragmentShaderID);
        GL20.glDeleteShader(vertexShaderID);
        GL20.glDeleteShader(fragmentShaderID);
        GL20.glDeleteProgram(programID);
    }

    protected abstract void bindAttributes();

    protected void bindAttribute(int attribute, String variableName) {
        // Bind the given attribute to the variable in the program
        GL20.glBindAttribLocation(programID, attribute, variableName);
    }

    private static int loadShader(String file, int type) {
        StringBuilder shaderSource = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                shaderSource.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int shaderId = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderId, shaderSource);
        GL20.glCompileShader(shaderId);

        if (GL20.glGetShaderi(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.out.println(GL20.glGetShaderInfoLog(shaderId, 500));
            System.exit(-1);
        }

        return shaderId;
    }
}
