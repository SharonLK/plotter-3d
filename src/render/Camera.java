package render;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import utils.MathUtils;

/**
 * Created by Sharon on 15-Dec-15.
 */
public class Camera {
    private Matrix4f perspectiveMatrix;
    private Matrix4f viewMatrix;
    private Vector3f position;
    private float pitch;
    private float roll;
    private float yaw;

    public Camera(Vector3f position, float pitch, float yaw, float roll, float aspect, float fovy, float zNear, float zFar) {
        this.position = position;
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;

        perspectiveMatrix = MathUtils.createPerspectiveMatrix(fovy, aspect, zNear, zFar);
        viewMatrix = MathUtils.createViewMatrix(this);
    }

    public Vector3f getForward() {
        return MathUtils.transformVector3f(new Vector3f(0, 0, -1), -pitch, -yaw);
    }

    public Vector3f getRight() {
        return MathUtils.transformVector3f(new Vector3f(1, 0, 0), -pitch, -yaw);
    }

    public Matrix4f getPerspectiveMatrix() {
        return perspectiveMatrix;
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
        viewMatrix = MathUtils.createViewMatrix(this);
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
        this.pitch = (this.pitch + 360) % 360;
        viewMatrix = MathUtils.createViewMatrix(this);
    }

    public float getRoll() {
        return roll;
    }

    public void setRoll(float roll) {
        this.roll = roll;
        this.roll = (this.roll + 360) % 360;;
        viewMatrix = MathUtils.createViewMatrix(this);
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
        this.yaw = (this.yaw + 360) % 360;;
        viewMatrix = MathUtils.createViewMatrix(this);
    }
}
