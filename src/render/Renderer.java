package render;

import environment.Light;
import model.Model;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import plot.display.Plot;
import render.shader.BasicShader;
import render.shader.PlotShader;
import utils.MathUtils;

import java.util.List;

/**
 * Created by Sharon on 14-Dec-15.
 */
public enum Renderer {
    ;

    private static final PlotShader plotShader = new PlotShader();
    private static final BasicShader basicShader = new BasicShader();

    private static Camera camera;

    public static void render(RenderableObject renderableObject) {
        if (camera == null) {
            return;
        }

        Model model = renderableObject.getModel();

        basicShader.start();

        basicShader.loadViewMatrix(camera.getViewMatrix());

        GL30.glBindVertexArray(model.getId());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);

        basicShader.loadModelMatrix(MathUtils.createModelMatrix(renderableObject.getPosition(),
                renderableObject.getRotation(),
                renderableObject.getScale()));
        GL11.glDrawElements(GL11.GL_TRIANGLES, model.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);

        basicShader.stop();
    }

    public static void renderPlot(Plot plot) {
        if (camera == null) {
            return;
        }

        Model model = plot.getModel();

        plotShader.start();

        plotShader.loadViewMatrix(camera.getViewMatrix());

        GL30.glBindVertexArray(model.getId());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        plotShader.loadModelMatrix(MathUtils.createModelMatrix(plot.getPosition(),
                plot.getRotation(),
                plot.getScale()));
        plotShader.loadPlotParameters(plot.getMinimumValue(), plot.getMaximumValue(),
                plot.getLevels());
        GL11.glDrawElements(GL11.GL_TRIANGLES, model.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

        GL20.glDisableVertexAttribArray(2);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);

        plotShader.stop();
    }

    public static void setCamera(Camera camera) {
        Renderer.camera = camera;

        plotShader.start();
        plotShader.loadPerspectiveMatrix(camera.getPerspectiveMatrix());
        plotShader.stop();

        basicShader.start();
        basicShader.loadPerspectiveMatrix(camera.getPerspectiveMatrix());
        basicShader.stop();
    }

    public static void loadLights(List<? extends Light> lights) {
        plotShader.start();
        plotShader.loadLights(lights);
        plotShader.stop();
    }

    public static void clean() {
        plotShader.clean();
    }
}
