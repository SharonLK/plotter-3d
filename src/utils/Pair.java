package utils;

/**
 * Created by Sharon on 16-Dec-15.
 */
public class Pair<S, T> {
    public final S first;
    public final T second;

    public Pair(S first, T second) {
        this.first = first;
        this.second = second;
    }
}
