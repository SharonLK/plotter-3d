package utils;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import render.Camera;

/**
 * Created by Sharon on 15-Dec-15.
 */
public enum MathUtils {
    ;

    public static Matrix4f createPerspectiveMatrix(float fovy, float aspect, float zNear, float zFar) {
        Matrix4f m = new Matrix4f();

        float f = 1 / (float) Math.tan(Math.toRadians(fovy / 2));

        m.m00 = f / aspect;
        m.m11 = f;
        m.m22 = (zFar - zNear) / (zNear - zFar);
        m.m23 = -1;
        m.m32 = (2 * zFar * zNear) / (zNear - zFar);
        m.m33 = 0;

        return m;
    }

    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f m = new Matrix4f();

        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), m, m);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), m, m);
        Matrix4f.rotate((float) Math.toRadians(camera.getRoll()), new Vector3f(0, 0, 1), m, m);

        Vector3f cameraPosition = camera.getPosition();
        m.translate(new Vector3f(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z));

        return m;
    }

    public static Matrix4f createModelMatrix(Vector3f position, Vector3f rotation, Vector3f scale) {
        Matrix4f m = new Matrix4f();

        m.translate(position);
        Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0, 0, 1), m, m);
        Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0, 1, 0), m, m);
        Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1, 0, 0), m, m);
        m.scale(scale);

        return m;
    }

    public static Vector3f transformVector3f(Vector3f src, float rx, float ry) {
        Matrix4f m = new Matrix4f();

        m.rotate((float) Math.toRadians(ry), new Vector3f(0, 1, 0));
        m.rotate((float) Math.toRadians(rx), new Vector3f(1, 0, 0));

        Vector4f transformed = Matrix4f.transform(m, new Vector4f(src.x, src.y, src.z, 1), null);

        return new Vector3f(transformed.x, transformed.y, transformed.z);
    }
}
