package environment;

import org.lwjgl.util.vector.Vector3f;

/**
 * Created by Sharon on 19-Dec-15.
 */
public class PointLight extends Light {
    public PointLight(Vector3f position, Vector3f color) {
        super(position, color);
    }
}
