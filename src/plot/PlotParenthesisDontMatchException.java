package plot;

/**
 * Created by Sharon on 16-Dec-15.
 */
public class PlotParenthesisDontMatchException extends Exception {
    @Override
    public String getMessage() {
        return "Amount of left parenthesis don't match amount of right parenthesis";
    }
}
