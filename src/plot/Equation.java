package plot;

import plot.operators.*;
import plot.operators.Number;
import utils.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Equation {
    private static final List<List<Pair<Character, Operator>>> operatorsPriority = new ArrayList<>();

    static {
        ArrayList<Pair<Character, Operator>> operatorList = new ArrayList<>();
        operatorList.add(new Pair<>('+', new Addition()));
        operatorList.add(new Pair<>('-', new Subtraction()));
        operatorsPriority.add(operatorList);

        operatorList = new ArrayList<>();
        operatorList.add(new Pair<>('*', new Multiplication()));
        operatorList.add(new Pair<>('/', new Division()));
        operatorsPriority.add(operatorList);

        operatorList = new ArrayList<>();
        operatorList.add(new Pair<>('^', new Power()));
        operatorsPriority.add(operatorList);
    }

    private EquationElement initialElement;

    /**
     * Constructs a new plot object, representing a plot built from the input equation. This object is able to compute
     * the equation values given specific x and y values.
     *
     * @param baseEquation The equation this plot will represent
     * @throws PlotParenthesisDontMatchException When the amount of left and right parenthesis don't match
     * @throws PlotParseException                When the syntax of the equation is invalid
     */
    public Equation(String baseEquation) throws PlotParenthesisDontMatchException, PlotParseException {
        String equation = baseEquation.toLowerCase().replaceAll(" ", "");

        initialElement = processEquation(equation);
    }

    /**
     * Computes the value of the plot at the given coordinates.
     *
     * @param x The x value of the coordinate.
     * @param y The y value of the coordinate.
     * @return The value of the plot at the given coordinate.
     */
    public float value(float x, float y) {
        return initialElement.value(x, y);
    }


    private EquationElement processEquation(String equation) throws PlotParenthesisDontMatchException, PlotParseException {
        for (List<Pair<Character, Operator>> operatorList : operatorsPriority) {
            int depth = 0;
            for (int i = 0; i < equation.length(); i++) {
                if (equation.charAt(i) == '(') {
                    depth++;
                } else if (equation.charAt(i) == ')') {
                    depth--;
                }

                if (depth == 0) {
                    for (Pair<Character, Operator> operatorPair : operatorList) {
                        if (equation.charAt(i) == operatorPair.first) {
                            return new EquationElement(new EquationElement[]{
                                    processEquation(equation.substring(0, i)),
                                    processEquation(equation.substring(i + 1))
                            }, operatorPair.second);
                        }
                    }
                }

                if (depth < 0) {
                    throw new PlotParenthesisDontMatchException();
                }
            }
        }

        while (equation.charAt(0) == '(') {
            equation = equation.substring(1, equation.length() - 1);
        }

        try {
            return new EquationElement(null, new Number(Float.parseFloat(equation)));
        } catch (NumberFormatException ignored) {

        }

        if (equation.equals("x")) {
            return new EquationElement(null, new Variable(true));
        } else if (equation.equals("y")) {
            return new EquationElement(null, new Variable(false));
        }

        return processEquation(equation);
    }
}
