package plot;

import model.Model;
import model.ModelsLoader;
import org.lwjgl.util.vector.Vector3f;
import plot.display.Plot;

/**
 * Created by Sharon on 15-Dec-15.
 */
public enum PlotProcessor {
    ;

    public static Plot getEquationModel(String equation, float xBegin, float xEnd, float xStep,
                                        float yBegin, float yEnd, float yStep, int levels,
                                        Vector3f position, Vector3f rotation, Vector3f scale) throws PlotParenthesisDontMatchException, PlotParseException {
        Equation plot = new Equation(equation);

        int xValues = (int) ((xEnd - xBegin) / xStep) + 1;
        int yValues = (int) ((yEnd - yBegin) / yStep) + 1;

        float[] positions = new float[xValues * yValues * 3];
        float[] normals1 = new float[xValues * yValues * 3];
        float[] normals2 = new float[xValues * yValues * 3];
        int[] indices = new int[(xValues - 1) * (yValues - 1) * 6];

        float[][] values = new float[xValues][yValues];
        float[][] valuesAux = new float[xValues + 2][yValues + 2];

        float min = Float.MAX_VALUE;
        float max = Float.MIN_VALUE;

        for (int i = -1; i < yValues + 1; i++) {
            for (int j = -1; j < xValues + 1; j++) {
                if (i == -1 || i == yValues || j == -1 || j == xValues) {
                    valuesAux[j + 1][i + 1] = plot.value(xBegin + j * xStep, yBegin + i * yStep);

                    continue;
                }

                values[j][i] = plot.value(xBegin + j * xStep, yBegin + i * yStep);
                valuesAux[j + 1][i + 1] = values[j][i];

                if (values[j][i] > max) {
                    max = values[j][i];
                }

                if (values[j][i] < min) {
                    min = values[j][i];
                }
            }
        }

        int idx = 0;

        for (int i = 0; i < yValues; i++) {
            for (int j = 0; j < xValues; j++) {
                float x = xBegin + j * xStep;
                float z = yBegin + i * yStep;

                Vector3f gradient1 = Vector3f.sub(new Vector3f(x - xStep, valuesAux[j][i], z),
                        new Vector3f(x + xStep, valuesAux[j + 2][i], z), null);
                Vector3f gradient2 = Vector3f.sub(new Vector3f(x, valuesAux[j][i], z - yStep),
                        new Vector3f(x, valuesAux[j][i + 2], z + yStep), null);
                Vector3f normal = Vector3f.cross(gradient2, gradient1, null);
                normal.normalise();

                positions[idx * 3] = x;
                positions[idx * 3 + 1] = values[j][i];
                positions[idx * 3 + 2] = z;

                normals1[idx * 3] = normal.x;
                normals1[idx * 3 + 1] = normal.y;
                normals1[idx * 3 + 2] = normal.z;

                normals2[idx * 3] = -normal.x;
                normals2[idx * 3 + 1] = -normal.y;
                normals2[idx * 3 + 2] = -normal.z;

                // Check if its not the last row or column of computation
                if (i < yValues - 1 && j < xValues - 1) {
                    indices[(idx - i) * 6] = idx;
                    indices[(idx - i) * 6 + 1] = idx + 1;
                    indices[(idx - i) * 6 + 2] = idx + xValues + 1;

                    indices[(idx - i) * 6 + 3] = idx;
                    indices[(idx - i) * 6 + 4] = idx + xValues + 1;
                    indices[(idx - i) * 6 + 5] = idx + xValues;
                }

                idx++;
            }
        }

        Model model = ModelsLoader.loadModel(positions, normals1, normals2, indices);

        return new Plot(model, position, rotation, scale, min, max, levels);
    }
}
