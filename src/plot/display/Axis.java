package plot.display;

import model.Model;
import model.ModelsLoader;
import org.lwjgl.util.vector.Vector3f;

/**
 * Created by Sharon on 23-Dec-15.
 */
public class Axis {
    private static Model axisModel;

    private Vector3f axisLength;

    public Axis(Vector3f axisLength) {
        if (Axis.axisModel == null) {
            loadAxisModel();
        }

        this.axisLength = axisLength;
    }

    public void render() {
        // TODO
    }


    private static void loadAxisModel() {
        axisModel = ModelsLoader.loadModel(new float[]{
                -0.5f, 0.5f, 0.5f,
                -0.5f, -0.5f, 0.5f,
                0.5f, -0.5f, 0.5f,
                0.5f, 0.5f, 0.5f,
                -0.5f, 0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, 0.5f, -0.5f,
        }, new int[]{
                0, 1, 2,
                0, 2, 3,
                3, 2, 6,
                3, 6, 7,
                7, 6, 5,
                7, 5, 4,
                4, 5, 1,
                4, 1, 0,
                4, 0, 3,
                4, 3, 7,
                2, 6, 5,
                2, 5, 1
        });
    }
}
