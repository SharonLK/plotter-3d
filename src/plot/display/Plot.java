package plot.display;

import model.Model;
import org.lwjgl.util.vector.Vector3f;
import render.RenderableObject;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Plot extends RenderableObject {
    private float minimumValue;
    private float maximumValue;
    private int levels;

    /**
     * Constructs a new plot object, containing the model, position, rotation and scale of the plot. In addition the
     * object contains the minimum and maximum values of the plot, used to determine the color of each point of the
     * plot. At last, the object also contains the number of different color 'levels' that will be displayed on the
     * plot. The amount of color levels determine the amount of different colors that will be used to draw the plot.
     *
     * @param model        The base model of the plot.
     * @param position     The position of the model in world coordinates.
     * @param rotation     The rotation of the model.
     * @param scale        The scale of the model.
     * @param minimumValue The minimum y value of the plot.
     * @param maximumValue The maximum y value of the plot.
     * @param levels       The amount of color levels used to draw the model.
     */
    public Plot(Model model, Vector3f position, Vector3f rotation, Vector3f scale, float minimumValue, float maximumValue, int levels) {
        super(model, position, rotation, scale);
        this.minimumValue = minimumValue;
        this.maximumValue = maximumValue;
        this.levels = levels;
    }

    /**
     * @return The minimum y value of the model.
     */
    public float getMinimumValue() {
        return minimumValue;
    }

    /**
     * @return The maximum y value of the model.
     */
    public float getMaximumValue() {
        return maximumValue;
    }

    /**
     * @return The amount of color levels used to draw the model.
     */
    public int getLevels() {
        return levels;
    }

    /**
     * Sets the new amount of color levels used to draw the model.
     *
     * @param levels The new amount of color levels.
     */
    public void setLevels(int levels) {
        this.levels = levels;
    }
}
