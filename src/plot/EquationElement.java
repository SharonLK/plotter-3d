package plot;

import plot.operators.Operator;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class EquationElement {
    private EquationElement[] children;
    private Operator operator;

    /**
     * Constructs a new plot element object, representing a node in the equation tree. Each node contains the operator
     * of the node and the children on which the operator will be applied when computing node's value.
     *
     * @param children The children of this node in the equation tree.
     * @param operator The operator this node represents.
     */
    public EquationElement(EquationElement[] children, Operator operator) {
        this.children = children;
        this.operator = operator;
    }

    /**
     * @param x The x value of the coordinate to compute.
     * @param y The y value of the coordinate to compute.
     * @return The value of this node at the given coordinate.
     */
    public float value(float x, float y) {
        if (children == null || children.length == 0) {
            return operator.calculate(x, y);
        }

        float[] values = new float[children.length];
        for (int i = 0; i < children.length; i++) {
            values[i] = children[i].value(x, y);
        }

        return operator.calculate(values);
    }
}
