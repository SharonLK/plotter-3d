package plot.operators;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Variable implements Operator {
    private boolean x;

    public Variable(boolean x) {
        this.x = x;
    }

    @Override
    public float calculate(float... values) {
        return x ? values[0] : values[1];
    }
}
