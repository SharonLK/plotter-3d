package plot.operators;

/**
 * Created by Sharon on 23-Dec-15.
 */
public class Power implements Operator {
    @Override
    public float calculate(float... values) {
        return (float) Math.pow(values[0], values[1]);
    }
}
