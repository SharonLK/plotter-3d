package plot.operators;

/**
 * Created by Sharon on 17-Dec-15.
 */
public class Subtraction implements Operator {
    @Override
    public float calculate(float... values) {
        return values[0] - values[1];
    }
}
