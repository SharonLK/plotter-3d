package plot.operators;

/**
 * Created by Sharon on 17-Dec-15.
 */
public class Division implements Operator {
    @Override
    public float calculate(float... values) {
        if (values[1] == 0) {
            return 0; // TODO: Throw exception
        }

        return values[0] / values[1];
    }
}
