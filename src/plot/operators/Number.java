package plot.operators;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Number implements Operator {
    private float num;

    public Number(float num) {
        this.num = num;
    }

    @Override
    public float calculate(float... values) {
        return num;
    }
}
