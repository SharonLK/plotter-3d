package plot.operators;

/**
 * Created by Sharon on 14-Dec-15.
 */
public interface Operator {
    /**
     * @param values The input values of the operator.
     * @return The result of applying the operator on the given values.
     */
    float calculate(float... values);
}
