package plot.operators;

/**
 * Created by Sharon on 14-Dec-15.
 */
public class Addition implements Operator {
    @Override
    public float calculate(float... values) {
        return values[0] + values[1];
    }
}
