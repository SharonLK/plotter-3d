package plot.operators;

/**
 * Created by Sharon on 16-Dec-15.
 */
public class Multiplication implements Operator {
    @Override
    public float calculate(float... values) {
        return values[0] * values[1];
    }
}
