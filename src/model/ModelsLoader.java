package model;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sharon on 14-Dec-15.
 */
public enum ModelsLoader {
    ;

    private static List<Integer> vaos = new ArrayList<>();
    private static List<Integer> vbos = new ArrayList<>();

    public static Model loadModel(float[] positions, int[] indices) {
        int id = createVAO();
        vaos.add(id);

        bindIndices(indices);
        storeVBO(0, 3, positions);

        unbindVAO();
        return new Model(id, indices.length);
    }

    public static Model loadModel(float[] positions, float[] colors, int[] indices) {
        int id = createVAO();
        vaos.add(id);

        bindIndices(indices);
        storeVBO(0, 3, positions);
        storeVBO(1, 3, colors);

        unbindVAO();
        return new Model(id, indices.length);
    }

    public static Model loadModel(float[] positions, float[] normals1, float[] normals2, int[] indices) {
        int id = createVAO();
        vaos.add(id);

        bindIndices(indices);
        storeVBO(0, 3, positions);
        storeVBO(1, 3, normals1);
        storeVBO(2, 3, normals2);

        unbindVAO();
        return new Model(id, indices.length);
    }


    private static int createVAO() {
        int vaoID = GL30.glGenVertexArrays();
        vaos.add(vaoID);
        GL30.glBindVertexArray(vaoID);

        return vaoID;
    }

    private static void unbindVAO() {
        // Unbinds the currently bound VAO
        GL30.glBindVertexArray(0);
    }

    private static void bindIndices(int[] indices) {
        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, convertToIntBuffer(indices), GL15.GL_STATIC_DRAW);
    }

    private static void storeVBO(int index, int elementSize, float[] data) {
        int vboID = GL15.glGenBuffers();
        vbos.add(vboID);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, convertToFloatBuffer(data), GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(index, elementSize, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }

    private static IntBuffer convertToIntBuffer(int[] data) {
        IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);

        // Finished writing to buffer and preparing it to be read
        buffer.flip();

        return buffer;
    }

    private static FloatBuffer convertToFloatBuffer(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);

        // Finished writing to buffer and preparing it to be read
        buffer.flip();

        return buffer;
    }

    public static void clean() {
        for (int i : vaos) {
            GL30.glDeleteVertexArrays(i);
        }

        for (int i : vbos) {
            GL15.glDeleteBuffers(i);
        }
    }
}
